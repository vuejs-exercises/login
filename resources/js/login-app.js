const app = new Vue({
    el: '#app',
    data: {
        user: '',
        password: '',
        logged: false,
        dbLS: []
    },
    methods: {
        login() {
            // Enviar datos del login al backend
            if(this.user != '' && this.password != '') {
                let response = compararDatos(this.user, this.password);
            } else {
                alert('Debe ingresar un usuario y contraseña para continuar');
            }
            
        },
        compararDatos: async function (u, p){
            console.log("usuario: "+u+", pass: "+p);
            const data = await fetch('resources/js/login.json');
            const usuarios = await data.json();

            usuarios.forEach(usuario => {
                if(usuario.user == u && usuario.password == p) {
                    logged = true;
                }             
            });

            if(logged) {       
                this.dbLS = {
                    user: this.user,
                    logged: logged
                };
                localStorage.setItem('kaos-library', JSON.stringify(this.dbLS));
                window.location.href = "http://127.0.0.1:5500/index.html";
            } else {
                alert("Usuario o contraseña inválidos");
            }
        }
    },
    created () {
        let datosLS = JSON.parse(localStorage.getItem('kaos-library'));
        if(datosLS === null) {
            this.dbLS = [];
        } else {
            if(datosLS.logged) {                
                window.location.href = "http://127.0.0.1:5500/index.html"
            } else {
                this.dbLS = datosLS;
                this.user = datosLS.user;
                this.logged = datosLS.logged;
            }
        }
    }
}) 